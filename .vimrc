syntax on
set nocp
set mouse=a
set number
filetype plugin indent on
set backspace=indent,eol,start
set hlsearch
set background=dark
let g:solarized_termcolors=256
let g:solarized_termtrans=1
colorscheme solarized
